import 'dart:async';

import 'package:coronaflutter/epidemy_calculator.dart';
import 'package:coronaflutter/general_data_card.dart';
import 'package:coronaflutter/rest_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static Dio _dio = Dio();
  final client = RestClient(_dio);
  final EpidemyCalculator _calculator = EpidemyCalculator();

  int _currentBottomBarIndex = 0;
  Timer _timer;

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  void initState() {
    super.initState();
    setup();
    _timer = Timer.periodic(Duration(seconds: 30), (t) {
      setup();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Coronavirus Live Data',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SafeArea(
        child: Scaffold(
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.black12,
            currentIndex: _currentBottomBarIndex,
            onTap: (index) {
              setState(() {
                _currentBottomBarIndex = index;
              });
            },
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.grid_on, color: Colors.white),
                  title: Text(
                    'General',
                    style: TextStyle(color: Colors.white),
                  )),
              BottomNavigationBarItem(
                  icon: Icon(Icons.list, color: Colors.white),
                  title: Text(
                    'Affected countries',
                    style: TextStyle(color: Colors.white),
                  ))
            ],
          ),
          body: _getBody(),
        ),
      ),
    );
  }

  List<Widget> _buildTopStats() {
    return [
      Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: InkWell(
              onTap: () {
                setState(() {
                  _calculator.sortByName();
                });
              },
              child: Text(
                'Country',
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: InkWell(
              onTap: () {
                setState(() {
                  _calculator.sortByTotalCases();
                });
              },
              child: Text(
                'Total cases',
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: InkWell(
              onTap: () {
                setState(() {
                  _calculator.sortByRecovered();
                });
              },
              child: Text(
                'Total recovered',
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: () {
                setState(() {
                  _calculator.sortByNewCases();
                });
              },
              child: Text(
                'New cases',
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
      SizedBox(
        height: 12,
      ),
      Row(
        children: [
          Expanded(
            child: Text(''),
            flex: 2,
          ),
          Expanded(
            flex: 2,
            child: Text(
              _calculator.getTotalCases(),
              style: TextStyle(color: Colors.red),
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              _calculator.getRecovered(),
              style: TextStyle(color: Colors.green),
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              _calculator.getNewCases(),
              style: TextStyle(color: Colors.redAccent),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    ];
  }

  Widget _getBody() {
    if (_calculator.getTotalCases() != 0) {
      if (_currentBottomBarIndex == 0) {
        return _getGeneralData();
      } else
        return _getDataList();
    }
    return Container(
      child: CircularProgressIndicator(
        strokeWidth: 3,
      ),
      alignment: Alignment.center,
    );
  }

  Widget _getGeneralData() {
    return Center(
      child: GridView.count(
        shrinkWrap: true,
        crossAxisSpacing: 2,
        mainAxisSpacing: 2,
        crossAxisCount: 2,
        children: <Widget>[
          GeneralDataCard(
            name: 'Total cases',
            value: _calculator.getTotalCases(),
            valueColor: Colors.red,
          ),
          GeneralDataCard(
            name: 'Death count',
            value: _calculator.getDeaths(),
            valueColor: Colors.red,
          ),
          GeneralDataCard(
            name: 'Total recovered',
            value: _calculator.getRecovered(),
            valueColor: Colors.green,
          ),
          GeneralDataCard(
            name: 'New cases',
            value: '+${_calculator.getNewCases()}',
            valueColor: Colors.redAccent,
          )
        ],
      ),
    );
  }

  Widget _getDataList() {s
    return Column(
      children: <Widget>[
        Card(
          child: Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                children: _buildTopStats(),
              )),
        ),
        Expanded(
            child: ListView(
          padding: EdgeInsets.all(2),
          children: _calculator.buildDataList(),
        )),
      ],
    );
  }

  void setup() async {
    client.getCoronas().then((coronas) async {
      await _calculator.setup(coronas);
      setState(() {});
    });
  }
}
