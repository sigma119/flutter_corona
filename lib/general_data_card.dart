import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class GeneralDataCard extends StatelessWidget {
  final String name;
  final String value;
  final Color valueColor;
  final Color titleColor;

  GeneralDataCard(
      {@required this.name,
      @required this.value,
      this.valueColor = Colors.black,
      this.titleColor = Colors.black});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24,
                color: titleColor
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text(value,
            style: TextStyle(
              fontSize: 18,
              color: valueColor
            ),)
          ],
        ),
      ),
    );
  }
}
