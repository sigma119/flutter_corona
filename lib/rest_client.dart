import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: 'http://79.188.41.153:3000')
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/corona")
  Future<List<CountryResponse>> getCoronas();
}

@JsonSerializable()
class CountryResponse {
  String country;
  String total;
  String newCases;
  String totalDeaths;
  String totalRecovered;
  String seriousCrit;

  CountryResponse({this.country, this.total, this.newCases, this.totalDeaths, this.totalRecovered, this.seriousCrit});

  factory CountryResponse.fromJson(Map<String, dynamic> json) => _$CountryFromJson(json);
  Map<String, dynamic> toJson() => _$CountryToJson(this);
}


class Country {
  String country;
  int total;
  int newCases;
  int totalDeaths;
  int totalRecovered;
  int seriousCrit;
  Country({this.country, this.total, this.newCases, this.totalDeaths, this.totalRecovered, this.seriousCrit});
}

