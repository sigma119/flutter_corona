import 'package:coronaflutter/rest_client.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EpidemyCalculator {
  int _totalDeaths = 0;
  int _totalRecovered = 0;
  int _totalCases = 0;
  int _newCases = 0;
  List<Country> _data = [];

  bool _sortByNameAscending = false;
  bool _sortByRecoveredAscending = false;
  bool _sortByTotalCasesAscending = false;
  bool _sortByNewCasesAscending = false;

  void resetSorts() {
    _sortByNameAscending = false;
    _sortByTotalCasesAscending = false;
    _sortByRecoveredAscending = false;
    _sortByNewCasesAscending = false;
  }

  void sortByTotalCases() {
    if (_sortByTotalCasesAscending)
      _data.sort((a, b) => b.total.compareTo(a.total));
    else
      _data.sort((a, b) => a.total.compareTo(b.total));

    _sortByNameAscending = false;
    _sortByRecoveredAscending = false;
    _sortByNewCasesAscending = false;
    _sortByTotalCasesAscending = !_sortByTotalCasesAscending;
  }

  void sortByRecovered() {
    if (_sortByRecoveredAscending)
      _data.sort((a, b) => b.totalRecovered.compareTo(a.totalRecovered));
    else
      _data.sort((a, b) => a.totalRecovered.compareTo(b.totalRecovered));

    _sortByNameAscending = false;
    _sortByRecoveredAscending = false;
    _sortByNewCasesAscending = false;
    _sortByRecoveredAscending = !_sortByRecoveredAscending;
  }

  void sortByNewCases() {
    if (_sortByNewCasesAscending)
      _data.sort((a, b) => b.newCases.compareTo(a.newCases));
    else
      _data.sort((a, b) => a.newCases.compareTo(b.newCases));

    _sortByTotalCasesAscending = false;
    _sortByRecoveredAscending = false;
    _sortByNewCasesAscending = false;
    _sortByNewCasesAscending = !_sortByNewCasesAscending;
  }

  void sortByName() {
    if (_sortByNameAscending)
      _data.sort((a, b) => a.country.compareTo(b.country));
    else
      _data.sort((a, b) => b.country.compareTo(a.country));

    _sortByTotalCasesAscending = false;
    _sortByRecoveredAscending = false;
    _sortByNewCasesAscending = false;

    _sortByNameAscending = !_sortByNameAscending;
  }

  String getDeaths() {
    if (_totalDeaths == 0) return 'loading';
    return _totalDeaths.toString();
  }

  String getTotalCases() {
    if (_totalCases == 0) return 'loading';
    return _totalCases.toString();
  }

  String getNewCases() {
    if (_newCases == 0) return 'loading';
    return _newCases.toString();
  }

  String getRecovered() {
    if (_totalRecovered == 0) return 'loading';
    return _totalRecovered.toString();
  }

  List<Widget> buildDataList() {
    List<Widget> list = [];

    if (_data.isEmpty) {
      list.add(Text('Loading'));
      return list;
    }

    _data.forEach((d) => list.add(Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(children: <Widget>[
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      d.country,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      d.total.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      d.totalRecovered.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.green),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      d.newCases.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.redAccent),
                    ),
                  ),
                ],
              )
            ]),
          ),
        )));
    return list;
  }

  Future setup(List<CountryResponse> data) async {
    var properData = data.map((item) {
      int total;
      int newCases;
      int totalDeaths;
      int totalRecovered;
      int seriousCrit;

      try {
        total = int.parse(item.total);
      } catch (e) {
        total = 0;
      }
      try {
        newCases = int.parse(item.newCases);
      } catch (e) {
        newCases = 0;
      }
      try {
        totalDeaths = int.parse(item.totalDeaths);
      } catch (e) {
        totalDeaths = 0;
      }
      try {
        totalRecovered = int.parse(item.totalRecovered);
      } catch (e) {
        totalRecovered = 0;
      }
      try {
        seriousCrit = int.parse(item.seriousCrit);
      } catch (e) {
        seriousCrit = 0;
      }

      return Country(
          country: item.country,
          total: total,
          newCases: newCases,
          totalDeaths: totalDeaths,
          totalRecovered: totalRecovered,
          seriousCrit: seriousCrit);
    });

    _data.clear();
    _data.addAll(properData);

    _data.sort((a, b) => b.total.compareTo(a.total));

    _totalDeaths = 0;
    _totalCases = 0;
    _totalRecovered = 0;
    _newCases = 0;

    _data.forEach((c) {
      _totalDeaths += c.totalDeaths;
      _totalRecovered += c.totalRecovered;
      _totalCases += c.total;
      _newCases += c.newCases;
    });
  }
}
