// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rest_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryResponse _$CountryFromJson(Map<String, dynamic> json) {
  return CountryResponse(
    country: json['country'] as String,
    total: json['total'] as String,
    newCases: json['newCases'] as String,
    totalDeaths: json['totalDeaths'] as String,
    totalRecovered: json['totalRecovered'] as String,
    seriousCrit: json['seriousCrit'] as String,
  );
}

Map<String, dynamic> _$CountryToJson(CountryResponse instance) => <String, dynamic>{
      'country': instance.country,
      'total': instance.total,
      'newCases': instance.newCases,
      'totalDeaths': instance.totalDeaths,
      'totalRecovered': instance.totalRecovered,
      'seriousCrit': instance.seriousCrit,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    this.baseUrl ??= 'http://79.188.41.153:3000';
  }

  final Dio _dio;

  String baseUrl;

  @override
  getCoronas() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<List<dynamic>> _result = await _dio.request('/corona',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    var value = _result.data
        .map((dynamic i) => CountryResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return Future.value(value);
  }
}
